package flatnet;

import static flatnet.Graph_Panel.infactor;
import static flatnet.Graph_Panel.outfactor;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * @author MultiTool
 */
public class MainGui {
  JFrame frame;
  DrawingPanel drawpanel;
  /* ********************************************************************************* */
  public MainGui() {
  }
  /* ********************************************************************************* */
  public void Init() {
    this.frame = new JFrame();
    this.frame.setTitle("FlatNetFork");
    this.frame.setSize(1000, 900);
    this.frame.addWindowListener(new WindowAdapter() {
      @Override public void windowClosing(WindowEvent e) {
        System.exit(0);
      }
    });
    Container contentPane = this.frame.getContentPane();
    this.drawpanel = new DrawingPanel();
    contentPane.add(this.drawpanel);
    this.drawpanel.BigApp = this;
    frame.setVisible(true);
  }
  /* ********************************************************************************* */
  public static class DrawingPanel extends JPanel implements MouseMotionListener, MouseListener, MouseWheelListener, ComponentListener, KeyListener {
    MainGui BigApp;
    int ScreenMouseX = 0, ScreenMouseY = 0;
    double MouseOffsetX = 0, MouseOffsetY = 0;
    Layers layers = null;
    int itcnt = 1;

    public static double infactor = 0.7;//0.999;// 
    public static double outfactor = 0.7;//0.999;// 
    boolean Learning_Mode_Signal = true;

    /* ********************************************************************************* */
    public DrawingPanel() {
      layers = new Layers();
      Init_Layers();
      this.Init();
    }
    /* ********************************************************************************* */
    public final void Init() {
      this.addMouseListener(this);
      this.addMouseMotionListener(this);
      this.addMouseWheelListener(this);
      this.addKeyListener(this);
    }
    /* ********************************************************************************* */
    public void Draw_Me(Graphics2D g2d) {
      // to do: create paramblob for drawing context, use it in things
      layers.Draw_3d(g2d);
      System.out.println("Draw_Me:" + this.itcnt);
    }
    /* ********************************************************************************* */
    public void Init_Layers() {
      double[] invec = new double[2];
      double outval = 0.0;
      for (int tcnt = 0; tcnt < 0; tcnt++) {
        Logic.Bit_To(tcnt, 0, 1, invec);
        boolean result = Training_Function(tcnt, 2);
        outval = result ? 1.0 : -1.0;
        outval *= outfactor;
        layers.Create_Path(invec, outval);
      }
      // create random paths
      for (int tcnt = 0; tcnt < 1; tcnt++) {
        Create_Random_Path();
      }
    }
    /* ********************************************************************************* */
    public void Create_Random_Path() {
      double[] invec = new double[2];
      double outval = 0.0;
      invec[0] = Logic.wheel.nextDouble() * 2.0 - 1.0;
      invec[1] = Logic.wheel.nextDouble() * 2.0 - 1.0;
      outval = Logic.wheel.nextDouble() * 2.0 - 1.0;
      layers.Create_Path(invec, outval);
    }
    /* ********************************************************************************* */
    public void Run_Layers_old() {
      if (!Learning_Mode_Signal) {
        layers.Set_Learning_Mode(false);
        Learning_Mode_Signal = true;
      }
      double outval = 0.0;
      for (int tcnt = 0; tcnt < 4; tcnt++) {
        double[] invec = new double[2];
        Logic.Bit_To(tcnt, 0, 1, invec);
        boolean result = Training_Function(tcnt, 2);
        outval = result ? 1.0 : -1.0;
        outval *= outfactor;
        System.out.println("outval:" + outval);
        layers.Run_Cycle(invec, outval);
        itcnt++;
        if (false) {
          layers.Collect_Desire_Backward();
        }
//        System.out.println("Run_Layers");
      }
    }
    /* ********************************************************************************* */
    public void Run_Layers() {
      if (!Learning_Mode_Signal) {
        layers.Set_Learning_Mode(false);
        Learning_Mode_Signal = true;
      }
      double outval = 0.0;

      int tcnt = itcnt % 4;
      double[] invec = new double[2];
      Logic.Bit_To(tcnt, 0, 1, invec);
      boolean result = Training_Function(tcnt, 2);
      outval = result ? 1.0 : -1.0;
      outval *= outfactor;
      System.out.println("outval:" + outval);
      layers.Run_Cycle(invec, outval);
      itcnt++;
      if (false) {
        layers.Collect_Desire_Backward();
      }
//      System.out.println("Run_Layers:" + tcnt);
//      if (this.itcnt > 30000) {
//        this.Learning_Mode_Signal = false;
//      }
    }
    /* ********************************************************************************* */
    public static boolean Training_Function(long bits, int bitlen) {
      switch (1) {
        case 0:
          return (bits & 1) == 1;// pass through
        case 1:
          return Logic.Multi_Xor(bits, bitlen);
        case 2:
          return Logic.Multi_And(bits, bitlen);
        case 3:
          return Logic.Multi_Or(bits);
        default:
          return false;
      }
    }
    /* ********************************************************************************* */
    @Override public void paintComponent(Graphics g) {
      super.paintComponent(g);
      Graphics2D g2d = (Graphics2D) g;
      Run_Layers();
      Draw_Me(g2d);// redrawing everything is overkill for every little change or move. to do: optimize this
      this.repaint();
    }
    /* ********************************************************************************* */
    @Override public void mouseDragged(MouseEvent me) {
    }
    @Override public void mouseMoved(MouseEvent me) {
      this.ScreenMouseX = me.getX();
      this.ScreenMouseY = me.getY();
    }
    /* ********************************************************************************* */
    @Override public void mouseClicked(MouseEvent me) {
    }
    @Override public void mousePressed(MouseEvent me) {
      this.repaint();
    }
    @Override public void mouseReleased(MouseEvent me) {
      this.repaint();
    }
    @Override public void mouseEntered(MouseEvent me) {
    }
    @Override public void mouseExited(MouseEvent me) {
    }
    /* ********************************************************************************* */
    @Override public void mouseWheelMoved(MouseWheelEvent mwe) {
      double XCtr, YCtr, Rescale;
      XCtr = mwe.getX();
      YCtr = mwe.getY();
      double finerotation = mwe.getPreciseWheelRotation();
      this.repaint();
    }
    /* ********************************************************************************* */
    @Override public void componentResized(ComponentEvent ce) {
    }
    @Override public void componentMoved(ComponentEvent ce) {
    }
    @Override public void componentShown(ComponentEvent ce) {
    }
    @Override public void componentHidden(ComponentEvent ce) {
    }
    /* ********************************************************************************* */
    @Override public void keyTyped(KeyEvent ke) {
    }
    @Override public void keyPressed(KeyEvent ke) {
      Learning_Mode_Signal = false;
    }
    @Override public void keyReleased(KeyEvent ke) {
    }
  }

}
