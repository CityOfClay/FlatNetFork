/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package flatnet;

import java.awt.*;
import java.util.ArrayList;

/**
 *
 * @author jcatkeson, February 2009
 */
public class Network {
  /* This will also function as a single layer in a layered network. */
  public ArrayList<Node_Base> nodelist = new ArrayList<Node_Base>();
  /* *************************************************************************************************** */
  public void Collect_And_Fire() {
    for (Node_Base node : nodelist) {
      node.Collect_And_Fire();
    }
  }
  /* *************************************************************************************************** */
  public void Distribute_Outfire() {
    for (Node_Base node : nodelist) {
      node.Distribute_Outfire();
    }
  }
  /* *************************************************************************************************** */
  public void Collect_Desire_Backward() {
    for (Node_Base node : nodelist) {
      node.Collect_Desire_Backward();
    }
  }
  /* *************************************************************************************************** */
  public void Run_Cycle() {
    boolean fullfire = Layers.fullfire;
    if (!fullfire) {
      this.Collect_Desire_Backward();
    }
    for (Node_Base node : nodelist) {
      node.Run_Cycle();
    }
    if (fullfire) {// attempt at fullfire
      this.Collect_Desire_Backward();
    }
  }
  /* *************************************************************************************************** */
  public double Full_Run_Cycle(double forcedfire) {
    int nodecnt = 0;// must be fullfire
    Node_Base node0 = this.nodelist.get(0);
    for (Node_Base node : nodelist) {
      node.Run_Cycle();
      nodecnt++;
    }
    this.Collect_Desire_Backward();
    if (node0.Learning_Mode) {
      node0.Set_Outfire(forcedfire);
    }
    this.Distribute_Outfire();
    return node0.outfire;
  }
  /* *************************************************************************************************** */
  public void Create_Nodes(int num_nodes, double xorg, double yorg) {
    double nodewdt = 220.0;
    for (int ncnt = 0; ncnt < num_nodes; ncnt++) {
      Node node = new Node();
      node.xorg = (int) xorg;
      node.yorg = (int) yorg;
      nodelist.add(node);
      xorg += nodewdt;
    }
  }
  /* *************************************************************************************************** */
  public void Create_Input_Nodes(int num_nodes, double xorg, double yorg) {
    double nodewdt = 220.0;
    for (int ncnt = 0; ncnt < num_nodes; ncnt++) {
      Node_Base node = new Node_Base();
      node.xorg = (int) xorg;
      node.yorg = (int) yorg;
      nodelist.add(node);
      xorg += nodewdt;
    }
  }
  /* *************************************************************************************************** */
  public void Create_Output_Nodes(int num_nodes, double xorg, double yorg) {
    double nodewdt = 220.0;
    for (int ncnt = 0; ncnt < num_nodes; ncnt++) {
      Node_Out node = new Node_Out();
      node.xorg = (int) xorg;
      node.yorg = (int) yorg;
      nodelist.add(node);
      xorg += nodewdt;
    }
  }
  /* *************************************************************************************************** */
  public void Connect_From_All(Network upstreamer) {// can connect two layers, or maybe one network to itself
    int num_us = upstreamer.nodelist.size();
    int num_ds = this.nodelist.size();
    for (int uscnt = 0; uscnt < num_us; uscnt++) {
      Node_Base usnode = upstreamer.nodelist.get(uscnt);
      for (int dscnt = 0; dscnt < num_ds; dscnt++) {
        Node_Base dsnode = this.nodelist.get(dscnt);
        dsnode.Add_Inlink(usnode);
      }
    }
  }
  /* *************************************************************************************************** */
  public void Create_Circle(int num_nodes, double xorg, double yorg) {
    Create_Nodes(num_nodes, xorg, yorg);
    int num_ds = this.nodelist.size();
    int uscnt = num_ds - 1;// last one
    int xloc = 0;
    int yloc = 0;
    int numcols = 6;
    double nodewdt = 150.0;
    double randamp = 0.25;// 5.1;
    Node_Base prevnode = this.nodelist.get(uscnt);
    for (int dscnt = 0; dscnt < num_ds; dscnt++) {
      Node_Base dsnode = this.nodelist.get(dscnt);
      xloc = (int) (nodewdt * (dscnt % numcols));
      yloc = (int) (nodewdt * (dscnt / numcols));
      dsnode.xorg = (int) xorg + xloc;
      dsnode.yorg = (int) yorg + yloc;
      Node_Base nextnode = this.nodelist.get((dscnt + 1) % num_ds);
      dsnode.Add_Inlink(prevnode);
      dsnode.Add_Inlink(nextnode);
      for (int pcnt = 0; pcnt < 1; pcnt++) {
        dsnode.Create_Random_Point();
      }
      ((Node) dsnode).planeform.Randomize(-randamp, randamp);
      prevnode = dsnode;
    }
  }
  /* *************************************************************************************************** */
  public void Create_Hypercube(int numdims, double xorg, double yorg) {
    int arraysize = 1 << numdims;
    Create_Nodes(arraysize, xorg, yorg);
    int xloc = 0;
    int yloc = 0;
    int numcols = 8;
    double nodewdt = 150.0;

    for (int cnt = 0; cnt < arraysize; cnt++) {
      Node_Base node = this.nodelist.get(cnt);

      xloc = (int) (nodewdt * (cnt % numcols));
      yloc = (int) (nodewdt * (cnt / numcols));
      node.xorg = (int) xorg + xloc;
      node.yorg = (int) yorg + yloc;

      int bitmask = 0;
      for (int bcnt = 0; bcnt < numdims; bcnt++) {
        bitmask = 1 << bcnt;
        int otherdex = cnt ^ bitmask; // xor flips one bit
        Node_Base othernode = this.nodelist.get(otherdex);
        node.Add_Inlink(othernode);
      }
    }
    for (int cnt = 0; cnt < arraysize; cnt++) {
      Node_Base node = this.nodelist.get(cnt);
      node.Create_Random_Point();
    }
  }
  /* *************************************************************************************************** */
  public void Create_All_To_All(int arraysize, double xorg, double yorg) {
    Create_Nodes(arraysize, xorg, yorg);
    int xloc = 0;
    int yloc = 0;
    int numcols = 6;
    double nodewdt = 150.0;

    for (int cnt = 0; cnt < arraysize; cnt++) {
      Node_Base node = this.nodelist.get(cnt);

      xloc = (int) (nodewdt * (cnt % numcols));
      yloc = (int) (nodewdt * (cnt / numcols));
      node.xorg = (int) xorg + xloc;
      node.yorg = (int) yorg + yloc;

      for (int bcnt = 0; bcnt < arraysize; bcnt++) {
        Node_Base othernode = this.nodelist.get(bcnt);
        node.Add_Inlink(othernode);
      }
    }
    for (int cnt = 0; cnt < arraysize; cnt++) {
      Node_Base node = this.nodelist.get(cnt);
      node.Create_Random_Point();
    }
  }
  /* *************************************************************************************************** */
  public void Create_Path() {// for initialization of dystal-style patches
    for (Node_Base node : nodelist) {
      node.Create_Path();
    }
  }
  /* *************************************************************************************************** */
  public void Draw_3d(Graphics2D gr) {
    for (Node_Base node : nodelist) {
      node.Draw_3d(gr);
    }
  }
  /* *************************************************************************************************** */
  public void Set_Learning_Mode(boolean Learning_Mode) {// for initialization of dystal-style patches
    for (Node_Base node : nodelist) {
      node.Learning_Mode = Learning_Mode;
    }
  }
  /* *************************************************************************************************** */
}
