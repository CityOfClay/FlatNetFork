/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package flatnet;

import java.util.ArrayList;
import java.awt.*;

/**
 *
 * @author jcatkeson, March 2009
 */
public class Node_Base {
  /* ****************************************************************************************************************************************************************************************************** */
  public static double infinity = (1.0e200);
  public static double fudge = (1.0 / infinity);
  /* *************************************************************************************************** */
  int ndims = 1;// 3 dimensions.  First dimension is a gimme, becuase it is the output/fire value.
  int ninputs = 0;// ndims - 1;
  public double maxdistance = 0;
  public double outfire = 0.0;
  public double planefire = 0.0;
  public int xorg,  yorg;// for plotting
  //public double rangemin,  rangectr,  rangemax;
  Bounder Bounds;
  double bounder_radius = 50.0;
  public boolean Learning_Mode = true;
  /* *************************************************************************************************** */
  public ArrayList<In_Link> inlinks = new ArrayList<In_Link>();
  public ArrayList<In_Link> outlinks = new ArrayList<In_Link>();
  /* *************************************************************************************************** */
  public Node_Base() {
    Bounds = new Bounder(bounder_radius, ndims);
  }
  /* *************************************************************************************************** */
  public void Run_Cycle() {
  }
  /* *************************************************************************************************** */
  public void Age_Points() {
  }
  /* *************************************************************************************************** */
  public void Create_Random_Point() {
  }
  /* *************************************************************************************************** */
  public void Collect_And_Fire() {
  }
  /* *************************************************************************************************** */
  public void Distribute_Outfire() {
    for (In_Link outlink : outlinks) {
      outlink.fireval = outfire;
    }
  }
  /* *************************************************************************************************** */
  public void Collect_Desire_Backward() {
  }
  /* *************************************************************************************************** */
  public void Apply_Desire_To_Closest(double desire) {
  }
  /* *************************************************************************************************** */
  public void Apply_Desire_To_Point(PointNd_Mov mover, double desire) {
    double color_change_lrate = 0.03;
    double newval = 0.0;
    newval = mover.loc[ninputs];
    double weight = 1.0;
    double delta = desire * color_change_lrate * weight;// snox
    newval = newval + delta;
    if (newval < -1.0) {// clip
      newval = -1.0;
    } else if (newval > 1.0) {
      newval = 1.0;
    }
    mover.loc[ninputs] = newval;
  }
  /* *************************************************************************************************** */
  public void Pass_Desire_Backward(PointNd bdesire) {
  }
  /* *************************************************************************************************** */
  public double Actfun(double rawval) {
    return rawval;
  }
  /* *************************************************************************************************** */
  public void Set_Outfire(double newoutfire) {
    this.outfire = newoutfire;
  }
  /* *************************************************************************************************** */
  public void Self_Adjust_Height(PointNd pnt, double corr) {
  }
  /* *************************************************************************************************** */
  public void Increase_Dims() {
    this.ndims++;
    this.ninputs = this.ndims - 1;
    Bounds = new Bounder(bounder_radius, ndims);
    maxdistance = 0;
    for (int cnt = 0; cnt < ninputs; cnt++) {
      maxdistance += 4;
    }
    maxdistance = Math.sqrt(maxdistance);
  }
  /* *************************************************************************************************** */
  public void Add_Inlink(Node_Base upstreamer) {
  }
  /* *************************************************************************************************** */
  public void Create_Path() {// for initialization of dystal-style patches
  }
  /* *************************************************************************************************** */
  public void Draw_3d(Graphics gr) {
    Graphics2D g2 = (Graphics2D) gr;
    float pntband = (float) (outfire);
    float red = pntband,   blue = (float) 1.0 - pntband;
    if (blue < 0) {
    } else if (blue > 1.0) {
      blue = 1;
    }
    if (red < 0) {
      red = 0;
    } else if (red > 1.0) {
      red = 1;
    }
    Color pnt_color = new Color(red, 0.0f, blue);
    g2.setColor(pnt_color);
    double rad = 25;
    g2.fillOval(xorg + (int) -rad, yorg + (int) -rad, (int) rad * 2, (int) rad * 2);
  }
  /* *************************************************************************************************** */
  public class Bounder {
    double[][] minmax;// = new double[2][ndims];
    public Bounder(double rad, int ndims) {
      minmax = new double[2][ndims];
      for (int dcnt = 0; dcnt < ndims; dcnt++) {
        this.minmax[0][dcnt] = -rad; //dimension min
        this.minmax[1][dcnt] = rad; //dimension max        
      }
    }
    public double Wdt() {
      return minmax[1][0] - minmax[0][0];
    }
    public double Hgt() {
      return minmax[1][1] - minmax[0][1];
    }
    public double Dep() {
      return minmax[1][2] - minmax[0][2];
    }
    public double Sz(int dim) {
      return minmax[1][dim] - minmax[0][dim];
    }
    public double Rad(int dim) {
      return Sz(dim) / 2.0;
    }
    public double CtrX() {
      return (minmax[1][0] + minmax[0][0]) / 2.0;
    }
    public double CtrY() {
      return (minmax[1][1] + minmax[0][1]) / 2.0;
    }
    public double CtrZ() {
      return (minmax[1][2] + minmax[0][2]) / 2.0;
    }
    public double Ctr(int dim) {
      return (minmax[1][dim] + minmax[0][dim]) / 2.0;
    }
  }
  /* *************************************************************************************************** */
  public class In_Link {
    public double fireval;
    public double desire;
    public  Node_Base upstreamer, downstreamer ;
  }
  /* *************************************************************************************************** */
  public static class PointNd {
    public int ndims = 3;
    public int ninputs = ndims - 1;
    public double[] loc = null;
    public PointNd(int num_dims) {
      ndims = num_dims;
      ninputs = ndims - 1;
      loc = new double[ndims];
      Clear();
    }
    public double getloc(int dim) {
      return loc[dim];
    }
    public void setloc(int dim, double value) {
      loc[dim] = value;
    }
    public void Add(PointNd other) {
      for (int cnt = 0; cnt < ndims; cnt++) {
        loc[cnt] += other.loc[cnt];
      }
    }
    public void Subtract(PointNd other) {
      for (int cnt = 0; cnt < ndims; cnt++) {
        loc[cnt] -= other.loc[cnt];
      }
    }
    public void Copy_From(PointNd other) {
      for (int cnt = 0; cnt < ndims; cnt++) {
        loc[cnt] = other.loc[cnt];
      }
    }
    public void Multiply(double factor) {
      for (int cnt = 0; cnt < ndims; cnt++) {
        loc[cnt] *= factor;
      }
    }
    public double Magnitude(int dimensions) {
      double sumsq = 0.0;
      for (int cnt = 0; cnt < dimensions; cnt++) {
        sumsq += loc[cnt] * loc[cnt];
      }
      return Math.sqrt(sumsq);
    }
    PointNd another;
    public double Get_Distance(PointNd other, int dimensions) {
      another = other;
      double delta,   dist = 0.0;// pythagorean distance
      for (int cnt = 0; cnt < dimensions; cnt++) {
        delta = other.loc[cnt] - this.loc[cnt];
        dist += delta * delta;// sum of the squares
      }
      dist = Math.sqrt(dist);
      return dist;
    }
    public double Get_Height(){
      return this.loc[ninputs];
    }
    //--------------------------------------------------------------------
    public double Magnitude_Squared(int dimensions) {
      double sumsq = 0.0;
      for (int cnt = 0; cnt < dimensions; cnt++) {
        sumsq += loc[cnt] * loc[cnt];
      }
      return sumsq;
    }/* Magnitude_Squared */

    public void Get_Delta(PointNd other, int dimensions, PointNd pdelta) {
      pdelta.Clear();
      double delta;
      for (int cnt = 0; cnt < dimensions; cnt++) {
        delta = other.loc[cnt] - this.loc[cnt];
        pdelta.loc[cnt] = delta;
      }
    }
    public double Dot_Product(PointNd other) {
      double retval = 0.0;// assume other is a unit vector.
      for (int cnt = 0; cnt < ndims; cnt++) {
        if ((this.loc[cnt] != 0.0) && (other.loc[cnt] != 0.0)) {// Zero always wins, even against infinity in this usage.
          retval += this.loc[cnt] * other.loc[cnt];
        }
      }
      return retval;
    }
    public void Unitize() { // convert to unit vector
      double length = 0.0;
      for (int cnt = 0; cnt < ndims; cnt++) {
        double axis = this.loc[cnt];
        length += axis * axis;
      }
      length = Math.sqrt(length);//pythagoran length
      if (length == 0.0) {// fudge to avoid divide-by-zero
        length = java.lang.Double.MIN_VALUE;
      }
      for (int cnt = 0; cnt < ndims; cnt++) {
        this.loc[cnt] /= length;
      }
    }
    public void Randomize(double minval, double maxval) {
      double range = maxval - minval;
      for (int cnt = 0; cnt < ndims; cnt++) {
        loc[cnt] = minval + (Logic.wheel.nextDouble() * range);
      }
    }
    public void Jitter(double minval, double maxval) {
      double range = maxval - minval;
      for (int cnt = 0; cnt < ndims; cnt++) {
        loc[cnt] += minval + (Logic.wheel.nextDouble() * range);
      }
    }
    public void Clear() {
      for (int cnt = 0; cnt < ndims; cnt++) {
        loc[cnt] = 0.0;
      }
    }
    public void CheckNAN() {
      for (int cnt = 0; cnt < ndims; cnt++) {
        if (loc[cnt] != loc[cnt]) {
          boolean noop = true;
        }
      }
    }
    public void CheckInf() {
      for (int cnt = 0; cnt < ndims; cnt++) {
        if (java.lang.Double.isInfinite(loc[cnt])) {
          boolean noop = true;
        }
      }
    }
    /* *************************************************************************************************** */
    public boolean CheckVert() {
      boolean flat = true;
      for (int dcnt = 0; dcnt < ninputs; dcnt++) {
        flat &= (this.loc[dcnt] == 0.0);
      }
      //flat &= (pnt.loc[ninputs] != 0.0);
      if (flat) {
        boolean noop = true;
      }
      for (int cnt = 0; cnt < ndims; cnt++) {
        if (this.loc[cnt] == 1.0) {// orthogonality test
          boolean noop = true;
        }
      }
      return flat;
    }
    /* *************************************************************************************************** */
    public void Get_Cross_Product(PointNd a, PointNd b) {
      this.Clear();
      this.loc[0] = (a.loc[1] * b.loc[2] - a.loc[2] * b.loc[1]);
      this.loc[1] = (a.loc[2] * b.loc[0] - a.loc[0] * b.loc[2]);
      this.loc[2] = (a.loc[0] * b.loc[1] - a.loc[1] * b.loc[0]);
    }
    /* *************************************************************************************************** */
    public void Normal_To_Plane(PointNd plane) {// take the normal, and get the formula of the plane (x y z), with respect to z (or last dimension)
      double height = this.loc[ninputs];
      if (height == 0.0) {
        height = java.lang.Double.MIN_VALUE;
      }
      for (int dimcnt = 0; dimcnt < ninputs; dimcnt++) {
        plane.loc[dimcnt] = (-this.loc[dimcnt] / height);// multiply each axis length by the slope for that axis
      }
    }
    /* *************************************************************************************************** */
    public void Get_Steepest(PointNd steep) {// get the steepest line on a plane with respect to Z by rotating its normal 90 degrees.
      steep.Clear();
      double vertical = this.loc[ninputs]; // last dimension is the output 'Z' dim.
      double floorpotenuse = 0.0;// first get the floorpotenuse, length of my shadow on floor
      for (int cnt = 0; cnt < ninputs; cnt++) {
        double axis = this.loc[cnt];
        floorpotenuse += axis * axis;
      }
      floorpotenuse = Math.sqrt(floorpotenuse);
      steep.loc[ninputs] = floorpotenuse;// the result 'Z' value is the normal's floorpotenuse
      for (int cnt = 0; cnt < ninputs; cnt++) {
        double axis = this.loc[cnt];
        double ratio = (axis / floorpotenuse);
        steep.loc[cnt] = -vertical * ratio;// will always point up if normal points up.
      }
    }
  }
  /* *************************************************************************************************** */
  public class PlaneNd extends PointNd {
    /* *************************************************************************************************** */
    public PlaneNd(int num_dims) {
      super(num_dims);
    }
    /* *************************************************************************************************** */
    public double Get_Height(PointNd pnt) {
      // get the height of this plane, at this point's coordinates
      double plane_hgt = this.loc[ninputs];// last dimension holds height offset
      double height = 0.0;
      for (int dim = 0; dim < ninputs; dim++) {
        height += (pnt.loc[dim] * this.loc[dim]);// multiply each axis length by the slope for that axis
      }
      height += plane_hgt;// add in the base offset
      return height;
    }
    /* *************************************************************************************************** */
    public void Plane_Ramp_To_Normal(PointNd norm) {// take the normal, and get the formula of the plane (x y z), with respect to z (or last dimension)
      for (int dimcnt = 0; dimcnt < ninputs; dimcnt++) {
        norm.loc[dimcnt] = (-this.loc[dimcnt]);
      }
      norm.loc[ninputs] = 1.0;// should return a normal above the plane
      norm.Unitize();
    }
  }
  /* *************************************************************************************************** */
  public class PointNd_Mov extends PointNd {
    public double freshness;
    public double stress;
    public double[] delta = null;
    public PointNd_Mov(int num_dims) {
      super(num_dims);
      delta = new double[ndims];
      freshness = 1.0;
      stress = 0.0;
      Clear();
    }
    public void Refresh() {
      freshness = 1.0;
    }
    public void Age() {
      //freshness *= 0.9;
      freshness *= 0.98;
    //freshness *= 0.99;
    }
    public boolean Is_Expired() {
      if (freshness < 0.1) {
        return true;
      } else {
        return false;
      }
    }
  }
}
